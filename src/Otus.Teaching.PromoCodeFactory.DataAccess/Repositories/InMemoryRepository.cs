﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }
        public Task<bool> DeleteByIdAsync(Guid guid)
        {
            if(guid != null)
            {
                var DataList = (ICollection<T>)Data;
                DataList.Remove(Data.FirstOrDefault(e => e.Id == guid));

                return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
        public Task<bool> UpdateByIdAsync(T data)
        {
            if(data != null)
            {
                var DataList = (ICollection<T>)Data;
                var editElem = DataList.FirstOrDefault(e => e.Id == data.Id);

                DataList.Remove(editElem);
                DataList.Add(data);

                return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
        public Task<bool> CreateAsync(T data)
        {
            if(data != null)
            {
                var dataList = (ICollection<T>)Data;
                dataList.Add(data); return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
    }
}